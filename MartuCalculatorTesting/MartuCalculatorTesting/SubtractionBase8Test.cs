﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class SubtractionBase8Test
    {
        ICalculator calc;

        public SubtractionBase8Test()
        {
            calc = new CalculatorBase8();
        }

        [TestCategory("SubtractionBase8"), TestMethod()]
        public void TestSubtractionPositiveValues()
        {
            Assert.AreEqual("144", calc.Substraction("310", "144"));
        }

        [TestCategory("SubtractionBase8"), TestMethod()]
        public void TestSubtractionNegativeValues()
        {
            Assert.AreEqual("-454", calc.Substraction("-310", "144"));
        }

        [TestCategory("SubtractionBase8"), TestMethod()]
        public void TestSubtractionNegativeValues2()
        {
            Assert.AreEqual("-144", calc.Substraction("144", "310"));
        }

        [TestCategory("SubtractionBase8"), TestMethod()]
        public void TestSubtractionEmptyValues()
        {
            Assert.AreEqual("-144", calc.Substraction("", "144"));
        }
    }
}
