﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class AdditionBase8Test
    {
        ICalculator calc;

        public AdditionBase8Test()
        {
            calc = new CalculatorBase8();
        }

        [TestCategory("AdditionBase8"), TestMethod()]
        public void TestAdditionPositiveValues()
        {
            Assert.AreEqual("322", calc.Addition("151", "151"));
        }

        [TestCategory("AdditionBase8"), TestMethod()]
        public void TestAdditionNegativeValues()
        {
            Assert.AreEqual("-144", calc.Addition("-310", "144"));
        }

        [TestCategory("AdditionBase8"), TestMethod()]
        public void TestAdditionEmptyValues()
        {
            Assert.AreEqual("151", calc.Addition("", "151"));
        }

        [TestCategory("AdditionBase8"), TestMethod()]
        public void TestAdditionEmptyValues2()
        {
            Assert.AreEqual("0", calc.Addition("", ""));
        }
    }
}
