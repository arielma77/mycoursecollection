﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class AdditionBase16Test
    {
        ICalculator calc;

        public AdditionBase16Test()
        {
            calc = new CalculatorBase16();
        }

        [TestCategory("AdditionBase16"), TestMethod()]
        public void TestAdditionPositiveValues()
        {
            Assert.AreEqual("1F4", calc.Addition("190", "64"));
        }

        [TestCategory("AdditionBase16"), TestMethod()]
        public void TestAdditionNegativeValues()
        {
            Assert.AreEqual("-12C", calc.Addition("-190", "64"));
        }

        [TestCategory("AdditionBase16"), TestMethod()]
        public void TestAdditionEmptyValues()
        {
            Assert.AreEqual("190", calc.Addition("", "190"));
        }

        [TestCategory("AdditionBase16"), TestMethod()]
        public void TestAdditionEmptyValues2()
        {
            Assert.AreEqual("0", calc.Addition("", ""));
        }
    }
}
