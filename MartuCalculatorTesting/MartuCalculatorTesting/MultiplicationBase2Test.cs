﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class MultiplicationBase2Test
    {
        ICalculator calc;

        public MultiplicationBase2Test()
        {
            calc = new CalculatorBase2();
        }

        [TestCategory("MultiplicationBase2"), TestMethod()]
        public void TestMultiplicationPositiveValues()
        {
            Assert.AreEqual("1100100", calc.Multiplication("10100", "101"));
        }

        [TestCategory("MultiplicationBase2"), TestMethod()]
        public void TestMultiplicationNegativeValues()
        {
            Assert.AreEqual("-1100100", calc.Multiplication("-10100", "101"));
        }

        [TestCategory("MultiplicationBase2"), TestMethod()]
        public void TestMultiplicationEmptyValues()
        {
            Assert.AreEqual("0", calc.Multiplication("", "101"));
        }
    }
}
