﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class DivisionBase2Test
    {
        ICalculator calc;

        public DivisionBase2Test()
        {
            calc = new CalculatorBase2();
        }

        [TestCategory("DivisionBase2"), TestMethod()]
        public void TestDivisionPositiveValues()
        {
            Assert.AreEqual("100", calc.Division("10100", "101"));
        }

        [TestCategory("DivisionBase2"), TestMethod()]
        public void TestDivisionNegativeValues()
        {
            Assert.AreEqual("-100", calc.Division("-10100", "101"));
        }

        [TestCategory("DivisionBase2"), TestMethod()]
        public void TestDivisionEmptyValues()
        {
            Assert.AreEqual("0", calc.Division("", "101"));
        }
    }
}
