﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class SubtractionBase12Test
    {
        ICalculator calc;

        public SubtractionBase12Test()
        {
            calc = new CalculatorBase12();
        }

        [TestCategory("SubtractionBase12"), TestMethod()]
        public void TestSubtractionPositiveValues()
        {
            Assert.AreEqual("420", calc.Substraction("568", "148"));
        }

        [TestCategory("SubtractionBase12"), TestMethod()]
        public void TestSubtractionNegativeValues()
        {
            Assert.AreEqual("-6B4", calc.Substraction("-568", "148"));
        }

        [TestCategory("SubtractionBase12"), TestMethod()]
        public void TestSubtractionNegativeValues2()
        {
            Assert.AreEqual("-420", calc.Substraction("148", "568"));
        }

        [TestCategory("SubtractionBase12"), TestMethod()]
        public void TestSubtractionEmptyValues()
        {
            Assert.AreEqual("-6B4", calc.Substraction("", "6B4"));
        }
    }
}
