﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class MultiplicationBase8Test
    {
        ICalculator calc;

        public MultiplicationBase8Test()
        {
            calc = new CalculatorBase8();
        }

        [TestCategory("MultiplicationBase8"), TestMethod()]
        public void TestMultiplicationPositiveValues()
        {
            Assert.AreEqual("144", calc.Multiplication("5", "24"));
        }

        [TestCategory("MultiplicationBase8"), TestMethod()]
        public void TestMultiplicationNegativeValues()
        {
            Assert.AreEqual("-144", calc.Multiplication("-5", "24"));
        }

        [TestCategory("MultiplicationBase8"), TestMethod()]
        public void TestMultiplicationEmptyValues()
        {
            Assert.AreEqual("0", calc.Multiplication("", "24"));
        }
    }
}
