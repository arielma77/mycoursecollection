﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class DivisionBase8Test
    {
        ICalculator calc;

        public DivisionBase8Test()
        {
            calc = new CalculatorBase8();
        }

        [TestCategory("DivisionBase8"), TestMethod()]
        public void TestDivisionPositiveValues()
        {
            Assert.AreEqual("24", calc.Division("144", "5"));
        }

        [TestCategory("DivisionBase8"), TestMethod()]
        public void TestDivisionNegativeValues()
        {
            Assert.AreEqual("-24", calc.Division("-144", "5"));
        }

        [TestCategory("DivisionBase8"), TestMethod()]
        public void TestDivisionEmptyValues()
        {
            Assert.AreEqual("0", calc.Division("", "144"));
        }
    }
}
