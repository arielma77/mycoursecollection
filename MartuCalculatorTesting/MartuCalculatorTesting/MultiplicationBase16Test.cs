﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class MultiplicationBase16Test
    {
        ICalculator calc;

        public MultiplicationBase16Test()
        {
            calc = new CalculatorBase16();
        }

        [TestCategory("MultiplicationBase16"), TestMethod()]
        public void TestMultiplicationPositiveValues()
        {
            Assert.AreEqual("708", calc.Multiplication("1E", "3C"));
        }

        [TestCategory("MultiplicationBase16"), TestMethod()]
        public void TestMultiplicationNegativeValues()
        {
            Assert.AreEqual("-708", calc.Multiplication("-1E", "3C"));
        }

        [TestCategory("MultiplicationBase16"), TestMethod()]
        public void TestMultiplicationEmptyValues()
        {
            Assert.AreEqual("0", calc.Multiplication("", "3C"));
        }
    }
}
