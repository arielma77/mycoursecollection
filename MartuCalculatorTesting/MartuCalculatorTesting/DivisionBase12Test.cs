﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class DivisionBase12Test
    {

        ICalculator calc;

        public DivisionBase12Test()
        {
            calc = new CalculatorBase12();
        }

        [TestCategory("DivisionBase12"), TestMethod()]
        public void TestDivisionPositiveValues()
        {
            Assert.AreEqual("63", calc.Division("526", "A"));
        }

        [TestCategory("DivisionBase12"), TestMethod()]
        public void TestDivisionNegativeValues()
        {
            Assert.AreEqual("-63", calc.Division("-526", "A"));
        }

        [TestCategory("DivisionBase12"), TestMethod()]
        public void TestDivisionEmptyValues()
        {
            Assert.AreEqual("0", calc.Division("", "526"));
        }
    }
}
