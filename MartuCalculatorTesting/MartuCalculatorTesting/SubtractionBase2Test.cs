﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class SubtractionBase2Test
    {
        ICalculator calc;

        public SubtractionBase2Test()
        {
            calc = new CalculatorBase2();
        }

        [TestCategory("SubtractionBase2"), TestMethod()]
        public void TestSubtractionPositiveValues()
        {
            Assert.AreEqual("1111", calc.Substraction("10100", "101"));
        }

        [TestCategory("SubtractionBase2"), TestMethod()]
        public void TestSubtractionNegativeValues()
        {
            Assert.AreEqual("-11001", calc.Substraction("-10100", "101"));
        }

        [TestCategory("SubtractionBase2"), TestMethod()]
        public void TestSubtractionNegativeValues2()
        {
            Assert.AreEqual("-1111", calc.Substraction("101", "10100"));
        }

        [TestCategory("SubtractionBase2"), TestMethod()]
        public void TestSubtractionEmptyValues()
        {
            Assert.AreEqual("-101", calc.Substraction("", "101"));
        }
    }
}
