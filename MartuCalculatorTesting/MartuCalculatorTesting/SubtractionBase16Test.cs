﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class SubtractionBase16Test
    {
        ICalculator calc;

        public SubtractionBase16Test()
        {
            calc = new CalculatorBase16();
        }

        [TestCategory("SubtractionBase16"), TestMethod()]
        public void TestSubtractionPositiveValues()
        {
            Assert.AreEqual("12C", calc.Substraction("190", "64"));
        }

        [TestCategory("SubtractionBase16"), TestMethod()]
        public void TestSubtractionNegativeValues()
        {
            Assert.AreEqual("-1F4", calc.Substraction("-190", "64"));
        }

        [TestCategory("SubtractionBase16"), TestMethod()]
        public void TestSubtractionNegativeValues2()
        {
            Assert.AreEqual("-12C", calc.Substraction("64", "190"));
        }

        [TestCategory("SubtractionBase16"), TestMethod()]
        public void TestSubtractionEmptyValues()
        {
            Assert.AreEqual("-12C", calc.Substraction("", "12C"));
        }
    }
}
