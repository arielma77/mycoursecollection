﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class DivisionBase16Test
    {
        ICalculator calc;

        public DivisionBase16Test()
        {
            calc = new CalculatorBase16();
        }

        [TestCategory("DivisionBase16"), TestMethod()]
        public void TestDivisionPositiveValues()
        {
            Assert.AreEqual("1E", calc.Division("708", "3C"));
        }

        [TestCategory("DivisionBase16"), TestMethod()]
        public void TestDivisionNegativeValues()
        {
            Assert.AreEqual("-1E", calc.Division("-708", "3C"));
        }

        [TestCategory("DivisionBase16"), TestMethod()]
        public void TestDivisionEmptyValues()
        {
            Assert.AreEqual("0", calc.Division("", "3C"));
        }
    }
}
