﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class NegativeTestingMultiBase
    {
        ICalculator calc;

        public NegativeTestingMultiBase()
        {
            calc = new CalculatorBase2();
        }

        #region Addition

        [TestCategory("NegativeTesting_Addition"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestAdditionIncorrectFormatSymbolsValues()
        {
            string result = calc.Addition("8(/)(&", "106");
        }

        [TestCategory("NegativeTesting_Addition"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestAdditionIncorrectFormatDecimalValues1()
        {
            string result = calc.Addition("-190", "64,5");
        }

        [TestCategory("NegativeTesting_Addition"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestAdditionIncorrectFormatDecimalValues2()
        {
            string result = calc.Addition("-190", "64.5");
        }

        #endregion 

        #region Division

        [TestCategory("NegativeTesting_Division"), TestMethod()]
        [ExpectedException(typeof(DivideByZeroException))]
        public void TestDivisionEmptyValues2()
        {
            string result = calc.Division("526", "0");
        }

        [TestCategory("NegativeTesting_Division"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestDivisionIncorrectFormatSymbolsValues()
        {
            string result = calc.Division("8(/)(&", "526");
        }

        [TestCategory("NegativeTesting_Division"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestDivisionIncorrectFormatDecimalValues1()
        {
            string result = calc.Division("8,5", "526");
        }

        [TestCategory("NegativeTesting_Division"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestDivisionIncorrectFormatDecimalValues2()
        {
            string result = calc.Division("8.5", "526");
        }

        #endregion 

        #region Multiplication

        [TestCategory("NegativeTesting_Multiplication"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMultiplicationIncorrectFormatSymbolsValues()
        {
            string result = calc.Multiplication("8(/)(&", "18A");
        }

        [TestCategory("NegativeTesting_Multiplication"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMultiplicationIncorrectFormatDecimalValues1()
        {
            string result = calc.Multiplication("8,5", "18A");
        }

        [TestCategory("NegativeTesting_Multiplication"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMultiplicationIncorrectFormatDecimalValues2()
        {
            string result = calc.Multiplication("8.5", "18A");
        }

        #endregion

        #region Subtraction

        [TestCategory("NegativeTesting_Subtraction"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestSubtractionIncorrectFormatSymbolsValues()
        {
            string result = calc.Multiplication("8(/)(&", "6B4");
        }

        [TestCategory("NegativeTesting_Subtraction"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestSubtractionIncorrectFormatDecimalValues1()
        {
            string result = calc.Multiplication("8,5", "6B4");
        }

        [TestCategory("NegativeTesting_Subtraction"), TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void TestSubtractionIncorrectFormatDecimalValues2()
        {
            string result = calc.Multiplication("8.5", "6B4");
        }

        #endregion
    }
}