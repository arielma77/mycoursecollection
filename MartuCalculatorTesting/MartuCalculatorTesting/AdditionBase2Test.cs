﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class AdditionBase2Test
    {
        ICalculator calc;

        public AdditionBase2Test()
        {
            calc = new CalculatorBase2();
        }

        [TestCategory("AdditionBase2"), TestMethod()]
        public void TestAdditionPositiveValues()
        {
            Assert.AreEqual("11001", calc.Addition("10100", "101"));
        }

        [TestCategory("AdditionBase2"), TestMethod()]
        public void TestAdditionNegativeValues()
        {
            Assert.AreEqual("-101", calc.Addition("-101", "0"));
        }

        [TestCategory("AdditionBase2"), TestMethod()]
        public void TestAdditionEmptyValues()
        {
            Assert.AreEqual("-101", calc.Addition("", "-101"));
        }

        [TestCategory("AdditionBase2"), TestMethod()]
        public void TestAdditionEmptyValues2()
        {
            Assert.AreEqual("0", calc.Addition("", ""));
        }
    }
}
