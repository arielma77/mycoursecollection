﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class AdditionBase12Test
    {
        ICalculator calc;

        public AdditionBase12Test()
        {
            calc = new CalculatorBase12();
        }

        [TestCategory("AdditionBase12"), TestMethod()]
        public void TestAdditionPositiveValues()
        {
            Assert.AreEqual("138", calc.Addition("106", "32"));
        }

        [TestCategory("AdditionBase12"), TestMethod()]
        public void TestAdditionNegativeValues()
        {
            Assert.AreEqual("-94", calc.Addition("-106", "32"));
        }

        [TestCategory("AdditionBase12"), TestMethod()]
        public void TestAdditionEmptyValues()
        {
            Assert.AreEqual("106", calc.Addition("", "106"));
        }

        [TestCategory("AdditionBase12"), TestMethod()]
        public void TestAdditionEmptyValues2()
        {
            Assert.AreEqual("0", calc.Addition("", ""));
        }
    }
}
