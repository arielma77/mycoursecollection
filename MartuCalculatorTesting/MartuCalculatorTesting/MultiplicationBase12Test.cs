﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorMultiBase;

namespace MartusCalculator
{
    [TestClass]
    public class MultiplicationBase12Test
    {
        ICalculator calc;

        public MultiplicationBase12Test()
        {
            calc = new CalculatorBase12();
        }

        [TestCategory("MultiplicationBase12"), TestMethod()]
        public void TestMultiplicationPositiveValues()
        {
            Assert.AreEqual("526", calc.Multiplication("18A", "3"));
        }

        [TestCategory("MultiplicationBase12"), TestMethod()]
        public void TestMultiplicationNegativeValues()
        {
            Assert.AreEqual("-526", calc.Multiplication("-18A", "3"));
        }

        [TestCategory("MultiplicationBase12"), TestMethod()]
        public void TestMultiplicationEmptyValues()
        {
            Assert.AreEqual("0", calc.Multiplication("", "18A"));
        }
    }
}
