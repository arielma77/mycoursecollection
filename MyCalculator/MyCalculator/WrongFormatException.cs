﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{

    public class WrongFormatException : FormatException
    {
        public WrongFormatException()
        {
        }

        public WrongFormatException(string message)
            : base(message)
        {
        }
    }
}
