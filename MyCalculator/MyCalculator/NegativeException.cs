﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class NegativeException : ArgumentException
    {
        public NegativeException()
        {
        }

        public NegativeException(string message)
            : base(message)
        {
        }
    }
}
