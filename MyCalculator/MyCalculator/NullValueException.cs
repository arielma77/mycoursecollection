﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class NullValueException : ArgumentOutOfRangeException
    {
        public NullValueException()
        {
        }

        public NullValueException(string message)
            : base(message)
        {
        }
    }
}
