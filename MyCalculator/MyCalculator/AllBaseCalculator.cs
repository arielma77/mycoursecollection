﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class CalculatorBase2 : Icalculator 
    {
        public string Addition(string a, string b)
        {
            int numberone = -1;
            int numbertwo = -1;

            //Format validations
            try
            {
                numberone = Convert.ToInt32(a, 2);
                numbertwo = Convert.ToInt32(b, 2);
            }
            catch (FormatException)
            {
                throw new WrongFormatException("\n Wrong format values");
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new NullValueException("\n Values cannot be empty");
            }
            catch (ArgumentException)
            {
                throw new NegativeException("\n Values cannot be negatives");
            }

            int result = numberone + numbertwo;
            return Convert.ToString(result, 2);
        }

        public string Subtraction(string a, string b)
        {
            int numberone = -1;
            int numbertwo = -1;

            //Format validations
            try
            {
                numberone = Convert.ToInt32(a, 2);
                numbertwo = Convert.ToInt32(b, 2);
            }
            catch (FormatException)
            {
                throw new WrongFormatException("Wrong format values");
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new NullValueException("Values cannot be empty");
            }
            catch (ArgumentException)
            {
                throw new NegativeException("Values cannot be negatives");
            }

            int result = numberone - numbertwo;
            return Convert.ToString(result, 2);
        }

        public string Multiplication(string a, string b)
        {
            int numberone = -1;
            int numbertwo = -1;

            //Format validations
            try
            {
                numberone = Convert.ToInt32(a, 2);
                numbertwo = Convert.ToInt32(b, 2);
            }
            catch (FormatException)
            {
                throw new WrongFormatException("Wrong format values");
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new NullValueException("Values cannot be empty");
            }
            catch (ArgumentException)
            {
                throw new NegativeException("Values cannot be negatives");
            }

            int result = numberone * numbertwo;
            return Convert.ToString(result, 2);
        }

        public string Division(string a, string b)
        {
            int numberone = -1;
            int numbertwo = -1;

            //Format validations
            try
            {
                numberone = Convert.ToInt32(a, 2);
                numbertwo = Convert.ToInt32(b, 2);
            }
            catch (FormatException)
            {
                throw new WrongFormatException("Wrong format values");
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new NullValueException("Values cannot be empty");
            }
            catch (ArgumentException)
            {
                throw new NegativeException("Values cannot be negatives");
            }

            int result = numberone / numbertwo;
            return Convert.ToString(result, 2);
        }
    }
}
