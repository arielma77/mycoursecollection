﻿using System;
using TechTalk.SpecFlow;

using CalculatorMultiBase;
using TechTalk.SpecFlow.Assist;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorSpecFlowTesting
{
    [Binding]
    public class AdditionSteps
    {
        private string result;

        private ICalculator cal;

        public AdditionSteps()
        {
            cal = new CalculatorBase2();
        }

        [Given(@"I have entered a (.*) into the calculator")]
        public void GivenIHaveEnteredAIntoTheCalculator(string number1)
        {
            ScenarioContext.Current.Add("firstNumber", number1);
        }
        
        [Given(@"I have also entered a (.*) into the calculator")]
        public void GivenIHaveAlsoEnteredAIntoTheCalculator(string number2)
        {
            ScenarioContext.Current.Add("secondNumber", number2);
        }
        
        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            //Retrieving 
            result = cal.Addition((string)ScenarioContext.Current["firstNumber"], (string)ScenarioContext.Current["secondNumber"]);
        }
        
        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(string expectedresult)
        {
            Assert.AreEqual(expectedresult, result);
        }
    }
}
