﻿Feature: Addition
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@Addition_PositiveTesting_Base2
Scenario Outline: Add two numbers
	Given I have entered a <number1> into the calculator
	And I have also entered a <number2> into the calculator
	When I press add
	Then the result should be <result> on the screen

	Examples: 
	| number1 | number2 | result |
	| 0       | 0       | 0      |
	| 1       | 0       | 1      |
	| -101    | 0       | -101   |
	| 1       | -101    | -100   |